import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  basePath = environment.baseUrl;

  constructor(
    private http: HttpClient,
    private router: Router
    ) { }

  login(authRequest: any): Observable<any> {
    return this.http.post<any>( this.basePath + '/login', authRequest ).pipe(
      tap( response => this.storeToken(response.token) )
    );
  }

  getResetOTP(authRequest: any): Observable<any> {
    return this.http.post<any>( this.basePath + '/getOtp', authRequest ).pipe(
      tap( response => this.storeToken(response.token) )
    );
  }

  validateOPT(authRequest: any): Observable<any> {
    return this.http.post<any>( this.basePath + '/validate/otp', authRequest ).pipe(
      tap( response => this.storeToken(response.token) )
    );
  }

  changePassword(authRequest: any): Observable<any> {
    return this.http.post<any>( this.basePath + '/changePasswordOtp', authRequest ).pipe(
      tap( response => this.storeToken(response.token) )
    );
  }

  storeToken(token) {
    localStorage.setItem('token', token);
  }

  getToken() {
    return localStorage.getItem('token');
  }

  logout() {
    localStorage.removeItem('token');
    this.router.navigate(['/login']);
  }
}
