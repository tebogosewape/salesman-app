import { Injectable } from '@angular/core';

import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse,
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';

import { catchError } from 'rxjs/operators';
import { AuthService } from './api/auth.service';

@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorService {

  constructor( private authService: AuthService ) { }

  handleHttpErrors(error: HttpErrorResponse) {

    const httpStatus = error.status;
    const httpErrors = error.error;
    const httpErrorMessage = error.message;

    if ( httpStatus === 403 ) {
      this.authService.logout();
    }

    return throwError({
      httpStatus, httpErrors, httpErrorMessage
    });

  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });

    const token = this.authService.getToken();

    if ( this.authService.getToken() !== null ) {
      headers = headers.append( 'Authorization',  'Bearer ' + token);
    }

    const clone = req.clone({headers});

    return next.handle(clone).pipe(
      catchError(this.handleHttpErrors)
    );

  }

}
